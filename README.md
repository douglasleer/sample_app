This is a demo application created by Dawei Li by following the Ruby on Rails Tutorial (by Michael Hartl). 

This application is developed in the Cloud9 IDE (https://c9.io/) and is deployed on the Heroku Platform (https://www.heroku.com/). 
The source code can be found at my BitBucket repository: https://bitbucket.org/douglasleer/sample_app, or my GitHub repository:
https://github.com/douglasleer/mini_post.

The application is running at https://sheltered-ravine-24358.herokuapp.com/. You are welcome to check it out!